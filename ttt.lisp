;;ttt.lisp
;; Play Tic-Tac-Toe!
;; Jackson Nelson

;; The initial board
(defparameter *brd* (list '- '- '- '- '- '- '- '- '-))
;; Keeps track of the last move
(defvar *move*)
;; Keeps track of the number of moves played
(defvar *i* 0)

;; Print a tic tac toe board
(defun print-board (board)
	(format t "=============")
	(do ((i 0 (+ i 1)) )
		((= i 9) nil)
		(if (= (mod i 3) 0)
			(format t "~%|")
			nil)
		(format t " ~A |" (nth i board))
	)	
	(format t "~%=============")
)

;; Tests whether all three items are equal
(defun threequal (a b c)
	(and (equal a b)
		 (equal b c)
	)
)
	

;; tests for victory
;; Returns true if three items are equal, except "-"
(defun victory (alist)
	(and (and (equal (first alist) (second alist))
		 (equal (second alist) (third alist))
	)
	(not (equal (first alist) '-))
	)
	
)

;; Checks each possible condition for a winning state
;; Returns nil not in a victory condition
;; Returns true if in victory
(defun check-victory (board)
	(if (victory (grab-col board 0)) t nil)
	(if (victory (grab-col board 1)) t nil)
	(if (victory (grab-col board 2)) t nil)
	(if (victory (grab-row board 0)) t nil)
	(if (victory (grab-row board 1)) t nil)
	(if (victory (grab-row board 2)) t nil)
	(if (victory (grab-diag1 board)) t nil)
	(if (victory (grab-diag2 board)) t nil)
)

;; Grab the nth row of tic tac toe
(defun grab-row (board row)
	(let ( (x (* 3 row)))
		(list (nth x board) (nth (+ 1 x) board) (nth (+ 2 x) board))
	)
)

;; Grab the nth col of tic tac toe
(defun grab-col (board col)
	(let ( (x (* 3 col)))
		(list (nth (+ x 0) board) (nth (+ x 3) board) (nth (+ x 6) board))
	)
)

;; Grabs the diagonal tiles 0-4-8
(defun grab-diag1 (board)
	(let ( (x (+ 0 0)))
		(list (nth (+ x 0) board) (nth (+ x 4) board) (nth (+ x 8) board))
	)
)

;; Grabs the diagonal tiles 2-4-6
(defun grab-diag2 (board)
	(let ( (x (+ 0 0)))
		(list (nth (+ x 2) board) (nth (+ x 4) board) (nth (+ x 6) board))
	)
)

;; Determines the number of occurrences of a specified tile (x or o)
;; If the board is null, returns 0
;; else if the element is the specified tile, increment the count, then check next tile.
(defun num-tile (i board)
  (cond
   ((null board) 0)
   ((equal i (car board)) (+ 1 (num-tile i (cdr board))))
   (t (num-tile i (cdr board)))
   )
 )

;; Allows a user to play a tile on the board. Play tile i at position j on board
;; i - the tile added ('x or 'o)
;; j - the position to be added at (0-8)
;; board - the board playing on
;; Doesn't allow playing on a tile already taken
(defun play-tile (i j board)
	(if (eql (nth j board) '-) (setf (nth j board) i) (format t "~%Tile already taken~%") )
	(print-board board)
)


;; Begins the simulation
;; Prints some formatting text
;; Then, loop:
;; while a player hasn't won (check-victory == nil), and fewer than 8 moves played, do the following
;; Prompt player 1 for tile number
;; Play tile
;; Prompt player 2 for tile, play tile
(defun play-game (board)
	;; Prints some starting text
	(format t "Play game!")
	(format t "~%Starting Board:~%")
	(print-board board)
	;; Main loop
	(loop while (and (null (check-victory board)) (> 8 *i*)) 
		do (format t "~%Player 1, enter tile number (0-8).~%")
			(setq *move* (read))
			(play-tile 'x *move* board)
			;;(if (check-victory board) (format t "~%Player 1 victory!~%") (format t "~%Next move~%" ) )
			(if (check-victory board) (player1-victory) (format t "~%Next move~%" ) )
			
			(format t "~%Player 2, enter tile number (0-8).~%")
			(setq *move* (read))
			(play-tile 'o *move* board)
			;;(if (check-victory board) (format t "~%Player 2 victory!~%") (format t "~%test~%" ) )
			(if (check-victory board) (player2-victory) (format t "~%Next move~%") )
			(+ *i* 1)
			
			;; Check if game is draw
			(check-draw *i* board)
	)
)

;; Prints when player 1 wins
;; Returns true
(defun player1-victory ()
	(format t "~%Player 1 victory!~%")
	t
)

;; Prints when player 2 wins
;; Returns true
(defun player2-victory ()
	(format t "~%Player 2 victory!~%")
	t
)

;; Prints when a game is a draw
;; Checks if number of moves is > 7 and if there is no victory, print "draw"
;; Returns nil if there is no draw
(defun check-draw (num_moves board)
	(if (and (< 7 num_moves) (null (check-victory board)))
		(format t "~%Game is a Draw!")
	)
	nil
)











