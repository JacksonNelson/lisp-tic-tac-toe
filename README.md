Lisp Tic-Tac-Toe - Jackson Nelson

This software will simulate a game of Tic-Tac-Toe for two players written in the Lisp programming language. This is accomplished through the use of several functions, including 
one which prints the current status of the board, one that tests if three given inputs are equal, one that tests if a player has won, one that lists the contents
of a specific row, and one that lists the contents of a specific column.

The main driver function of this program is the play-game function, which takes the board *brd* as its input.

This folder will contain one Lisp file for handling the creation and progression of the tic-tac-toe game, this README document, a software design document word file,
and a PDF of the SDD.

Author: Jackson Nelson. nelsonj6@augsburg.edu